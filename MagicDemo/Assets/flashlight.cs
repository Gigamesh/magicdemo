﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flashlight : MonoBehaviour
{

	public GameObject hand;

	private scaretrigger scaretrigger = null;
	private BodyBagTrigger bodyBagTrigger;

	// Use this for initialization
	void Start ()
	{
		transform.parent = hand.transform;
		transform.localPosition = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		RaycastHit hit;
		// Does the ray intersect any objects excluding the player layer
		if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
		{
			Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
			Debug.Log(hit.collider.gameObject.name);
			if (hit.collider.name == "MonsterScare")
			{
				scaretrigger = hit.collider.GetComponent<scaretrigger>();
				if (scaretrigger != null)
				{
					scaretrigger.Lit();
				}
			}

			if (hit.collider.name == "BodyScare")
			{
				bodyBagTrigger = hit.collider.GetComponent<BodyBagTrigger>();
				if (bodyBagTrigger != null)
				{
					bodyBagTrigger.Lit();
				}
			}
		}

	}
}
