﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class LevelMechanic : MonoBehaviour
{

	public GameObject key;
	public GameObject disappearingFX;
	public GameObject[] keyPositions;
	public Transform player;
	public Animator levelSequence;

	private Vector3 playerPos;
	private int currentPosition;

	void Start ()
	{
		key.transform.position = keyPositions[0].transform.position;
		transform.position = key.transform.position;
		keyPositions[0].SetActive(true);
	}

	void Update()
	{
		 playerPos = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
		key.transform.LookAt(playerPos);
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<Hand>())
		{
			PlayLevel();

			if (currentPosition != keyPositions.Length -1)
			{
				disappearingFX.transform.position = keyPositions[currentPosition].transform.position;
				disappearingFX.SetActive(true);
				disappearingFX.transform.LookAt(playerPos);
				keyPositions[currentPosition].SetActive(false);

				currentPosition++;
				key.transform.position = keyPositions[currentPosition].transform.position;
				transform.position = key.transform.position;
				
			}

			keyPositions[currentPosition].SetActive(true);
		}
	}

	private void OnTriggerExit(Collider other)
	{
		Debug.Log("Exited");
	}

	void PlayLevel()
	{
		if (!levelSequence.enabled)
			levelSequence.enabled = true;
	}

}
