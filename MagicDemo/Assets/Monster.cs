﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{

	public Renderer monsterRenderer;

	public Animator monsterAnimator;

	// Update is called once per frame
	void Update ()
	{
		if (monsterRenderer.isVisible) monsterAnimator.SetBool("IsMoving", true);

		else if (monsterAnimator.GetCurrentAnimatorStateInfo(0).IsName("Moving"))
			monsterAnimator.SetBool("IsMoving", true);

		else monsterAnimator.SetBool("IsMoving", false);



		if (monsterAnimator.GetCurrentAnimatorStateInfo(0).IsName("Moving"))
		{
			if (transform.position.z >= 300)
			{
				monsterAnimator.SetBool("ReachedLimit", true);
			}

			transform.Translate(Vector3.forward * Time.deltaTime * 35);
		}
	}
}
