﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderArmSpawn : MonoBehaviour
{

	public GameObject[] oldArm;

	public GameObject spiderArm;

	// Use this for initialization
	void Start ()
	{
		StartCoroutine(tranform());
	}
	

	IEnumerator tranform()
	{
		yield return new WaitForSeconds(15);
		foreach (GameObject old in oldArm)
		{
			old.SetActive(false);
		}
		spiderArm.SetActive(true);
	}
}
