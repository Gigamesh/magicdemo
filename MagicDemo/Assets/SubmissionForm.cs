﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Text;
using UnityEngine.SceneManagement;

public class SubmissionForm : MonoBehaviour
{
	public GameObject[] questions;
	public LevelManager levelManager;

	private List<string[]> rowData = new List<string[]>();
	private int currentQuestion;

	// Use this for initialization
	void Start()
	{
		currentQuestion = 0;
		foreach (GameObject question in questions)
		{
			question.SetActive(false);
		}
		questions[currentQuestion].SetActive(true);
	}


	public void NextQuestion()
	{
		questions[currentQuestion].SetActive(false);
		currentQuestion++;
		questions[currentQuestion].SetActive(true);
	}

	public void Submit()
	{
		levelManager.NextScene();
		Save();
		questions[currentQuestion].SetActive(false);
	}

	void Save(){

		string[] rowDataTemp = new string[9];

		if (!System.IO.File.Exists(getPath()))
		{

			rowDataTemp[0] = "Scene/Emotion";

			int columnNumberCreation = 1;

			foreach (GameObject question in questions)
			{
				var slider = question.GetComponentInChildren<Slider>();

				if (slider != null)
				{
					rowDataTemp[columnNumberCreation] = slider.name;
					columnNumberCreation++;
				}
			}

			rowData.Add(rowDataTemp);
		}

		rowDataTemp = new string[9];

		rowDataTemp[0] = SceneManager.GetActiveScene().name;
		int columnNumber = 1;

		foreach (GameObject question in questions)
		{
			
			var slider = question.GetComponentInChildren<Slider>();

			if (slider != null)
			{
				float sliderValue = slider.value;
				rowDataTemp[columnNumber] = sliderValue.ToString();
				columnNumber++;
			}
			
		}
		rowData.Add(rowDataTemp);

		string[][] output = new string[rowData.Count][];

		for(int i = 0; i < output.Length; i++){
			output[i] = rowData[i];
		}

		int     length         = output.GetLength(0);
		string     delimiter     = ",";

		StringBuilder sb = new StringBuilder();
        
		for (int index = 0; index < length; index++)
			sb.AppendLine(string.Join(delimiter, output[index]));
        
        
		string filePath = getPath();

		Debug.Log(sb);
		StreamWriter outStream = System.IO.File.AppendText(filePath);
		outStream.Write(sb);
		outStream.Close();
	}

	// Following method is used to retrive the relative path as device platform
	private string getPath(){
#if UNITY_EDITOR
		return Application.dataPath +"/CSV/"+"Saved_data.csv";
#elif UNITY_ANDROID
        return Application.persistentDataPath+"Saved_data.csv";
#elif UNITY_IPHONE
        return Application.persistentDataPath+"/"+"Saved_data.csv";
#else
        return Application.dataPath +"/"+"Saved_data.csv";
#endif
	}
}
