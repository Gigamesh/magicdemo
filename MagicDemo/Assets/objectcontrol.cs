﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectcontrol : MonoBehaviour
{

	public GameObject[] _object;
	public bool state;


	void Update()
	{
		if (state) SetActive();

		if (!state) SetInactive();
	}
	public void SetActive()
	{
		foreach (var obj in _object)
		{
			if (obj != null)
				obj.SetActive(true);
		}
		
	}

	public void SetInactive()
	{
		foreach (var obj in _object)
		{
			obj.SetActive(false);
		}
	}
}
