﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.InteractionSystem;
using Input = UnityEngine.Input;

public class LevelManager : MonoBehaviour
{
	public int SceneNumber;
	public GuidingLightMovement guidingLight;
	public GameObject fade;


	void Start()
	{
		StartCoroutine(FadeIntoScene(fade.GetComponentInChildren<Image>()));
	}

	public void NextScene()
	{

		StartCoroutine(FadeOutOfScene(fade.GetComponentInChildren<Image>()));

	}

	void Update()
	{

		if (Input.GetButtonDown("Monster"))
		{
			SceneManager.LoadScene(0);
		}

		if (Input.GetButtonDown("NoMonster"))
		{
			SceneManager.LoadScene(1);
		}

		if (Input.GetButtonDown("Exit"))
		{
			Debug.Log("Exiting");
			Application.Quit();
		}
	}

	public void SlowTime()
	{
		if (Time.timeScale == 1.0f)
		{
			guidingLight.SlowModeInitiated();
			Time.timeScale = 0.1f;
		}

		else
		{
			guidingLight.SlowModeEnded();
			Time.timeScale = 1.0f;
		}
	}

	private IEnumerator FadeOutOfScene(Image fadeImage)
	{
		fade.SetActive(true);
		for (byte ft = 0; ft <= 254; ft += 1)
		{
			fadeImage.color = new Color32(0,0,0,ft);
			yield return null;
		}

		if (SceneNumber == 6)
		{
			SceneNumber = -1;
		}

		if (SceneNumber == 7)
		{
			SceneNumber = -2;
		}

		SceneManager.LoadSceneAsync(SceneNumber+2);
	}

	private IEnumerator FadeIntoScene(Image fadeImage)
	{
		for (byte ft = 255; ft >= 1; ft -= 1)
		{
			fadeImage.color = new Color32(0,0,0,ft);
			yield return null;
		}
		fade.SetActive(false);
	}
}