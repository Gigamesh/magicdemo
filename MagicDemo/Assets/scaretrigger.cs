﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class scaretrigger : MonoBehaviour
{

	public Renderer newRenderer;
	public Animator animator;
	public AudioSource roar;


	public void Lit()
	{
		if (newRenderer.isVisible)
		{
			if (animator != null)
			{
				animator.enabled = true;
			}

			StartCoroutine(Destroy());
			if (!roar.isPlaying)
			{
				roar.Play();
			}
			
		}
	}

	IEnumerator Destroy()
	{
		yield return new WaitForSeconds(1);
		Destroy(gameObject);
	}
}
