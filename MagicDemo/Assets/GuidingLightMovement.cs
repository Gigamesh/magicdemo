﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuidingLightMovement : MonoBehaviour
{

	public Transform playerLightPos;
	public Transform doorLightPos;
	public Transform doorKnob;
	public Transform player;

	public float rotationSpeed;
	public float speed;
	public bool guide;

	private float oldSpeed;
	private float oldRotationSpeed;

	public bool grabAttention;
	public float lightAngle;
	private float currentLightAngle;

	void Start()
	{
		currentLightAngle = GetComponent<Light>().spotAngle;
		transform.position = playerLightPos.position;
		transform.LookAt(player.position);
	}

	// Update is called once per frame
	void Update()
	{
		if (guide)
		{
			var targetRotation = Quaternion.LookRotation(doorKnob.position - transform.position, Vector3.up);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);

			float step = speed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, doorLightPos.position, step);
		}

		if (grabAttention && currentLightAngle > lightAngle)
		{
			currentLightAngle = GetComponent<Light>().spotAngle;
			GetComponent<Light>().spotAngle -= Time.deltaTime * 20f;

		}
	}

	public void InSight()
	{
		grabAttention = true;
	}

	public void LookedAt()
	{
		guide = true;
	}

	public void NotInSight()
	{
		grabAttention = false;
		guide = false;
	}

	public void SlowModeInitiated()
	{
		oldRotationSpeed = rotationSpeed;
		oldSpeed = speed;

		rotationSpeed *= 10f;
		speed *= 10f;
	}

	public void SlowModeEnded()
	{
		rotationSpeed = oldRotationSpeed;
		speed = oldSpeed;

	}
}
