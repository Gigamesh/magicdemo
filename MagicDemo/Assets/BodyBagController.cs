﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyBagController : MonoBehaviour
{

	public GameObject body;
	public BodyJump[] jumps;
	public AudioSource bodyWriggle;

	private Renderer newRenderer;

	void Start()
	{
		if (gameObject.GetComponent<Renderer>()) newRenderer = gameObject.GetComponent<Renderer>();
	}

	void Update () {

		if (newRenderer != null)
		{
			if (newRenderer.isVisible)
			{
				EnableBody();
			}
		}
		

		if (!bodyWriggle.isPlaying)
		{
			foreach (var bodyJump in jumps)
			{
				bodyJump.StopAllCoroutines();
				bodyJump.enabled = false;
			}
		}

		else
		{
			foreach (var bodyJump in jumps)
			{
				bodyJump.enabled = true;
			}
		}
		
	}

	void EnableBody()
	{
		if(!body.activeInHierarchy) body.SetActive(true);
	}
}
