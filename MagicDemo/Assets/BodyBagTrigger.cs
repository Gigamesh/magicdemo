﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyBagTrigger : MonoBehaviour {

	public Renderer newRenderer;
	public Animator animator;
	public AudioSource roar;


	public void Lit()
	{
		if (newRenderer.isVisible)
		{
			if (animator != null)
			{
				animator.enabled = true;
			}

			if (!roar.isPlaying)
			{
				roar.Play();
			}
			
		}
	}

}
