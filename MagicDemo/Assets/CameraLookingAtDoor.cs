﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLookingAtDoor : MonoBehaviour
{
	public GuidingLightMovement lightScript;
	// Update is called once per frame

	Renderer m_Renderer;

	void Start()
	{
		m_Renderer = GetComponent<Renderer>();
	}

	void Update ()
	{
		if (m_Renderer.isVisible)
		{
			lightScript.InSight();
			lightScript.LookedAt();
		}
		else
		{
			lightScript.NotInSight();
		}
	}
}
