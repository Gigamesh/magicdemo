﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class SimpleMovement : MonoBehaviour
{

	public Hand[] hands;
	public LevelManager level;

	public GameObject player;
	public float speed = 1f;

	private bool movementDisabled;

	// Update is called once per frame
	void Update () {

		if (Input.GetButtonDown("DisableMovement"))
		{
			movementDisabled = !movementDisabled;
			Debug.Log("MovementChanged");
		}

		if (!movementDisabled)
		{

			foreach (Hand hand in hands)
			{
				if (hand.controller == null)
				{
					continue;
				}

				if (hand.controller.GetPress(EVRButtonId.k_EButton_Grip))
				{
					if (hand.name == "MasterHandRight")
					{
						player.transform.Translate(Vector3.forward * Time.deltaTime * speed);
					}

					if (hand.name == "MasterHandLeft")

					{
						player.transform.Translate(Vector3.back * Time.deltaTime * speed);
					}
				}

				if (hand.controller.GetPress(EVRButtonId.k_EButton_ApplicationMenu))
				{
					if (hand.name == "MasterHandRight")
					{
						player.transform.Translate(Vector3.right * Time.deltaTime * speed);
					}

					if (hand.name == "MasterHandLeft")

					{
						player.transform.Translate(Vector3.left * Time.deltaTime * speed);
					}
				}
			}
		}


	}
}
