﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inactivate : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		StartCoroutine(Inactivate());
	}

	IEnumerator Inactivate()
	{
		yield return new WaitForSeconds(3);
		gameObject.SetActive(false);
	}
}
