﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InactiveTimer : MonoBehaviour {

	void Start()
	{
		StartCoroutine(Inactive());
	}

	IEnumerator Inactive()
	{
		// suspend execution for 5 seconds
		yield return new WaitForSeconds(3);
		GetComponent<Button>().interactable = true;
		
	}
}
