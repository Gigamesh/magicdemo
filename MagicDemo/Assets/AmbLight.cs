﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbLight : MonoBehaviour {

	public float ambientLight;


	void Update() {
		{
            RenderSettings.ambientIntensity = this.ambientLight;
        }
    }
 
}
