﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyJump : MonoBehaviour
{

	public float jumpForce = 0.3f;

	void Update ()
	{
		float waitTime = Random.Range(0f, 7f);
		StartCoroutine(Jump(waitTime));
	}

	IEnumerator Jump(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);
		gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up*jumpForce, ForceMode.Impulse);
	}
}
