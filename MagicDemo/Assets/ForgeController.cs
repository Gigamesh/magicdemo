﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ForgeController : MonoBehaviour
{

	public HingeJoint[] joints;
	public ParticleSystem smoke;
	public AudioSource screams;

	float statTime;
//	public bool trigger;

//	void Update()
//	{
//		if (trigger)
//		{
//			TriggerDoor();
//			trigger = false;
//		}
//			
//	}
	
	public void TriggerDoor()
	{
		
		foreach (var joint in joints)
		{
			statTime = Random.Range(0f, 0.3f);
			StartCoroutine(TriggerDoorMechanic(joint,statTime));
		}

		StartSecondarySystems(statTime);
	}

	void StartSecondarySystems(float soundDelay)
	{
		if (!smoke.isPlaying) smoke.Play();
		else smoke.Stop();

		if(!screams.isPlaying) screams.PlayDelayed(soundDelay);
		else screams.Stop();
	}

	IEnumerator StopMotor(HingeJoint joint)
	{
		yield return new WaitForSeconds(0.1f);
		joint.useMotor = false;
	}

	IEnumerator TriggerDoorMechanic(HingeJoint joint, float startTime)
	{
		yield return new WaitForSeconds(startTime);
		JointMotor motor = joint.motor;
		motor.targetVelocity = -1*joint.motor.targetVelocity;
		joint.motor = motor;

		joint.useMotor = true;

		JointLimits limits = joint.limits;
		limits.bounciness = limits.bounciness < 0.5f ? 0.5f : 0f;

		joint.limits = limits;
		StartCoroutine(StopMotor(joint));

		
	}
}
