﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForgeClankSound : MonoBehaviour
{


	public AudioSource ClankSound;


	void OnTriggerEnter(Collider other)
	{

			ClankSound.volume = 0.8f;
			if (other.GetComponent<Rigidbody>() != null) ClankSound.volume *= other.GetComponent<Rigidbody>().velocity.magnitude;
			if (ClankSound.volume < 0.3) ClankSound.volume = 0.3f;
			if (ClankSound.volume < 0.1) ClankSound.volume = 0f;
			ClankSound.Play();
	}

}