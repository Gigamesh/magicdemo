﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderAssist : MonoBehaviour {

	public int sliderPos;
	public Slider slider;

	public void SetSliderPosition()
	{
		slider.value = sliderPos;
	}
}
