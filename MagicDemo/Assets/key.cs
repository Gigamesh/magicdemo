﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class key : MonoBehaviour {

	private Hand hand = null;
	public DoorKnobController doorKnob;
	public ParticleSystem [] handPower;
	public GameObject guidingLight;

	private void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<Hand>())
		{
			hand = other.GetComponent<Hand>();
		}
	}

	private void OnTriggerExit(Collider other)
	{
		//Debug.Log("Exited");
		hand = null;
	}

	// Update is called once per frame
	void Update () {

		if (hand != null)
		{
			if (hand.controller.GetPress(EVRButtonId.k_EButton_SteamVR_Trigger))
			{
				doorKnob.keyAcquired = true;
				hand.controller.TriggerHapticPulse(5000);
				StartCoroutine(KeyAcquisition());
				guidingLight.SetActive(true);
				foreach (ParticleSystem power in handPower)
				{
					power.Play();
				}
			}
		}


	}

	IEnumerator KeyAcquisition()
	{
		yield return new WaitForSeconds(0.5f);
		gameObject.SetActive(false);
	}
}
