﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderArm : MonoBehaviour {

	// Use this for initialization
	public Animator spiderAnimator;
	public Transform target;
	public float speed;

	private Transform originalTarget;
	void Start()
	{
		originalTarget = target;
	}
	// Update is called once per frame
	void Update ()
	{

		if (spiderAnimator.GetCurrentAnimatorStateInfo(0).IsName("Run")) 
		{
			run();
		}

	}

	void run()
	{
		if (target.transform.position.magnitude - transform.position.magnitude < 10)
		{
			target = Randomize(target);
		}

		if (target.transform.position.magnitude - originalTarget.transform.position.magnitude < 0)
		{
			target = originalTarget;
		}

		transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
	}

	private Transform Randomize(Transform randTarget)
	{
		randTarget.position += new Vector3(Random.Range(-25f, 25f), 0, Random.Range(-25f, 25f));
		return randTarget;
	}
}
