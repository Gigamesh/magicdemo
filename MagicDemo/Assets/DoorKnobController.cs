﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class DoorKnobController : MonoBehaviour
{

	public GameObject doorPlate;
	public SubmissionForm form;
	public bool keyAcquired;

	private Animator doorAnimator;
	private Hand hand = null;

	// Use this for initialization
	void Start ()
	{
		doorAnimator = doorPlate.GetComponent<Animator>();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<Hand>())
		{
			hand = other.GetComponent<Hand>();
		}
	}

	private void OnTriggerExit(Collider other)
	{
		Debug.Log("Exited");
		hand = null;
	}

	void Update()
	{
		if (hand != null)
		{
			if (hand.controller.GetPress(EVRButtonId.k_EButton_SteamVR_Trigger))
			{
				if (keyAcquired)
				{
					doorAnimator.SetBool("knobTriggered", true);
					form.gameObject.SetActive(true);
					hand.controller.TriggerHapticPulse(2000);
					gameObject.SendMessage("Destroy",null,SendMessageOptions.DontRequireReceiver);
				}
				
			}
		}

	}
}
