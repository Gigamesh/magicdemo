﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrematoriumOnly : MonoBehaviour
{

	public GameObject[] objects;

	public void Destroy()
	{
		foreach (var o in objects)
		{
			Destroy(o);
		}
	}

}
