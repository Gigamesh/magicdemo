﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomController : MonoBehaviour
{

	public ForgeController[] forges;

	void ActivateForges()
	{
		foreach (var forgeController in forges)
		{
			forgeController.TriggerDoor();
		}
	}
}
