﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodySoundController : MonoBehaviour
{

	public AudioSource bodySplat;
	public AudioSource bodyWriggle;
	public AudioSource breath;

	private bool triggered;

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer == 15)
		{
			breath.Pause();
			PlaySequence();
		}
		
	}

	void PlaySequence()
	{
		if (!triggered)
		{
			if (!bodySplat.isPlaying)
			{
				bodySplat.Play();
				StartCoroutine(Wriggle(bodySplat.clip.length));
			}

			triggered = true;
		}
	}

	IEnumerator Wriggle(float splatLength)
	{
		yield return new WaitForSeconds(splatLength + 0.1f);
		bodyWriggle.Play();
	}
}
