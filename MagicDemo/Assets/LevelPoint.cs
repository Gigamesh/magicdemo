﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class LevelPoint : MonoBehaviour
{

	public Animator levelAnimator;
	public String[] boolNames;


	void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<Hand>())
		{
			foreach (String currentName in boolNames)
			{
				levelAnimator.SetBool(currentName, true);
			}
		}
	}
}
