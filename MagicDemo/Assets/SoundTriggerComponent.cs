﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundTriggerComponent : MonoBehaviour {

	public AudioSource Sound;
	public bool randomizePitch;

	void OnTriggerEnter(Collider other)
	{
		if (other.tag != "NoSound")
		{
			if (randomizePitch)
			{
				Sound.pitch = Random.Range(0.8f, 1.2f);
			}
			Sound.Play();
		}
	
	}
}
