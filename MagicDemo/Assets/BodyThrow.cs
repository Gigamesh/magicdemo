﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyThrow : MonoBehaviour
{

	public float force;
	public Rigidbody[] rigidbodies;

	void Start ()
	{
		foreach (var rb in rigidbodies)
		{
			float tempForce = force;
			force = Random.Range(force - 2f, force + 2f);
			rb.AddForce(-0.5f * force, 0.5f *force, 0f, ForceMode.VelocityChange);
			force = tempForce;
		}
		
	}
}
