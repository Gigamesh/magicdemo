﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContinuousSound : MonoBehaviour
{

	private Rigidbody rb;
	public AudioSource soundClip;
	private int caseNumber;

	public AudioClip[] sounds;

	public bool pitchRandomizer;

	// Use this for initialization
	void Start ()
	{
		rb = GetComponent<Rigidbody>();
		
	}
	
	// Update is called once per frame
	void Update () {

		if (rb.velocity.magnitude > 0.1f)
		{
			caseNumber = Mathf.RoundToInt(Random.Range(0f, sounds.Length-1));

			//soundClip.pitch *= rb.velocity.magnitude;
			//soundClip.volume *= rb.velocity.magnitude;

			if (!soundClip.isPlaying) {

				if (pitchRandomizer)
				{
					soundClip.pitch = Random.Range(0.3f, 1f);
				}
				soundClip.clip = sounds[caseNumber];
				soundClip.Play();
				Debug.Log(gameObject + ":" + caseNumber);
			}
			 
		}
	}
}
