﻿using UnityEngine;
using System.Collections;

// Applies an explosion force to all nearby rigidbodies
public class Explosion : MonoBehaviour
{
    public float radius = 500.0f;
    public float power = 1000.0F;

    public inactivate group;

    void Start()
    {
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (hit.gameObject.layer == 14)
            {
	            if (rb != null)
	            {
		            hit.isTrigger = false;
		            rb.isKinematic = false;
		            rb.useGravity = true;

		            rb.AddExplosionForce(power, explosionPos, radius);

	            }
            }
            
                
			
        }

        group.enabled = true;

    }
}